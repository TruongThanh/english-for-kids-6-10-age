/* Home page - alert */
$(document).ready(function(e) {
    /*$('img[usemap]').rwdImageMaps();*/
    $('area').on('click', function() {
        alert(' Hello ' + $(this).attr('alt') );
    });
    var yourAudio = new Array(document.getElementById('audio-b'), document.getElementById('audio-d'), document.getElementById('audio-a'), document.getElementById('audio-e'), document.getElementById('audio-f'), document.getElementById('audio-cat'));
    var ctr = new Array(document.getElementById('audioControl-b'), document.getElementById('audioControl-d'), document.getElementById('audioControl-a'), document.getElementById('audioControl-e'), document.getElementById('audioControl-f'), document.getElementById('audioControl-cat'));
    var audio = new Array();
    for (i = 0; i < 7; i++) {
        audio[i] = yourAudio[i], ctr[i], playButton = document.getElementById('play'),
        pauseButton = document.getElementById('pause');
    }

    function play_music(i) {
        ctr[i].onclick = function() {
            if (audio[i].paused) {
                audio[i].play();
            } else {
                audio[i].pause();
            }
            toggleButton();
            // Prevent Default Action
            return false;
        };
    }

    function toggleButton() {
        if (playButton.style.display === 'none') {
            playButton.style.display = 'block';
            pauseButton.style.display = 'none';
        } else if (playButton.style.display != 'none') {
            playButton.style.display = 'none';
            pauseButton.style.display = 'block';
        } else if (currentTime == duration) {
            playButton.style.display = 'none';
            pauseButton.style.display = 'block';
        }
    }

    for (i = 0; i < 7; i++) {
        play_music(i);
    }
});